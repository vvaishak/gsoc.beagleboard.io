.. _gsoc-2015-projects:

:far:`calendar-days` 2015
#########################

Demo Android app using BBBAndroid
**********************************

.. youtube:: 3A9nzUyZhK0
   :width: 100%

| **Summary:** BBBAndroid is mainly designed to enable users to run Android on our favorite embedded linux board (i.e. Beaglebone Black). "Demo Android app using BBBAndroid" project focuses on the demonstration of apps that can run on Android which makes use of some awesome peripherals support of the BeagleBone Black like ADC, GPIO, I2C, SPI, USB, CAN , PWM, UART etc. Future task could be to make simple APIs for interfacing BBB peripherals using NDK.

**Contributor:** Ankur Kumar Yadav

**Mentors:** Andrew Henderson, Anuj Deshpande

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2015/orgs/beagleboard/projects/ankur_a_yadav.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/ankurayadav/bbbandroidHAL/wiki
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub wiki


BeaglePilot 2.0: Making Underwater Drones
******************************************

.. youtube:: GvIZOCGMYiA
   :width: 100%

| **Summary:** 

**Contributor:** Rohith Madhavan

**Mentors:** Víctor Mayoral Vilches, Iñigo Muguruza, Alejandro Hernández

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2015/orgs/beagleboard
         :color: light
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://beaglepilot2.wordpress.com/
         :color: primary
         :shadow:
         :expand:

         :fab:`wordpress;pst-color-light` - WordPress blog

Android-based Remote Display
*****************************

.. youtube:: gJABSa_SLFs
   :width: 100%

| **Summary:** My project goal is to implement a kernel device driver for USB Playback device, USB mouse and keyboard using Android AOA protocol. Successful completion of this project will give support of multiple cape and peripherals in a single gadget. I'll also focus on improving the USB framebuffer driver and develop corresponding android application for end user.

**Contributor:** Azizul Hakim

**Mentors:** Praveen Kumar Pendyala, Vlad Ungureanu, Vladimir Pantelic

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2015/orgs/beagleboard/projects/azizulhakim.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/azizulhakim/beagleusb/wiki
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub wiki

BeagleSat - Enabling accurate magnetic field measurements on CubeSats
**********************************************************************

.. youtube:: ojDqtxv9oGY
   :width: 100%

| **Summary:** Magnetic field measurements play an important role in Earth-orbiting satellites, such as attitude determination and scientific instrument pointing. Unless built specifically for high accuracy measurements, satellites usually come with significant magnetic sources of errors that severely degrade measurement accuracy. This GSoC project aims to implement algorithms that enable low-cost high quality magnetic field measurements on smaller spacecraft without booms using the BeagleBone platform.

**Contributor:** Niko Visnjic

**Mentors:** Steve Arnold, Alexander Hiam, Kumar Abhishek

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2015/orgs/beagleboard/projects/niko_visnjic.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/nvisnjic/BeagleSat/wiki
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub repo

NW.js (a.k.a node-webkit) based cross-platform getting-started app
*******************************************************************

.. youtube:: XVZS8kLhhwY
   :width: 100%

| **Summary:** The cross-platform getting-started app will provide a tool for new users, that allows an easy and fast configuration of Beagle boards. Inexperienced users get the chance to start using Beagle quickly and most importantly hassle-free, allowing them to experience Linux and embedded systems.

**Contributor:** Ariane Paola Gomes

**Mentors:** Jason Kridner, Tim Orling  

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2015/orgs/beagleboard/projects/apgomes.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/arianepaola/beaglebone-getting-started/tree/nwjs
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub repo

PRUSS support for newer kernels
********************************

.. youtube:: 5YgbVmFl65Y
   :width: 100%

| **Summary:** The aim is to build a framework for easy communication between the ARM and PRUSS. Currently these features are provided by libprussdrv and linux kernel’s remoteproc infrastructure. However both have their limitation and require developers to hack kernel drivers to optimize them for their application. This project would develop a lightweight, robust easy-to-use, yet powerful communication framework for ARM-PRU, which would make life for beaglebone users a lot easier.

**Contributor:** Shubhangi Gupta   

**Mentors:** Pantelis Antoniou, Kumar Abhishek, Hunyue Yau

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2015/orgs/beagleboard/projects/shubhi1407.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/shubhi1407/PRU-framework/wiki
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub wiki

PRU-Bridge
***********

.. youtube:: KyChK_NQLvA
   :width: 100%

| **Summary:** The aim of this project is to create a generic, multi channel bridge between userspace Linux and PRU, allowing developers to send and receive data seamlessly between ARM <--- > PRU via creation of plugin driver.

**Contributor:** Apaar Gupta

**Mentors:** Alexander Hiam, Deepak Karki, Hunyue Yau  

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2015/orgs/beagleboard/projects/apaar.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/Apaar/PRU-Bridge
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub repo

Bone101
********

| **Summary:** The primary objective of this project is to maximize the utilization of BoneScript, enhancing the user-friendliness of Bone101, particularly for beginners. The focus is on streamlining processes such as uploading, downloading, and sharing tutorials to ensure a seamless and accessible experience. The overarching goal is to simplify these operations, catering to newcomers and making the platform more user-friendly for a broader audience.

**Contributor:** ehab albadawy

**Mentors:** Jason Kridner, Diego Turcios

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://www.google-melange.com/archive/gsoc/2015/orgs/beagleboard/projects/ehabb.html
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://github.com/ebadawy/bone101
         :color: primary
         :shadow:
         :expand:

         :fab:`github;pst-color-light` - GitHub wiki

.. tip:: 

   Checkout eLinux page for GSoC 2015 projects `here <https://elinux.org/BeagleBoard/GSoC/2015_Projects>`_ for more details.
