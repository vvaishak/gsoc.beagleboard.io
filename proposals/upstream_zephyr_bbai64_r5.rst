.. _gsoc-proposal-template:

Proposal template 
#################

Introduction
*************

This proposal is to add upstream Zephyr RTOS support on ARM Cortex-R5 core in J721E SoC used in BBAI-64 single board computer.
During the GSoC development timeline, the goal is to add peripheral driver support for McSPI, MailBox (inter processor communication),
and provide basic IPC examples to achieve communication between the Linux cores (ARM A72 Core) and the Zephyr running core (ARM R5F Core). 
The implementation is to enable device manager to use TI-SCI to control features like turn on/off the clock, power management on 
ARM R5F core in the SoC.


Summary links
=============

- **Contributor:** `Vaishak Vidhyadharan <https://forum.beagleboard.org/u/vaishak_vidhyadharan>`_
- **Mentors:** `Nishanth Menon <https://forum.beagleboard.org/u/nishanth_menon>`_, `Dhruv Gole <https://forum.beagleboard.org/u/dhruvag2000>`_
- **Code:** *TODO*
- **Documentation:** `Vaishak Vidhyadharan / docs.beagleboard.io · GitLab <https://openbeagle.org/vvaishak/docs.beagleboard.io>`_
- **GSoC:** `None`

Status
=======

This project is currently just a proposal.

Proposal
========

* Created accounts on OpenBeagle.org, Discord and Beagle Forum
* Raised the PR for cross compiling and running gsoc application `PR#188 <https://github.com/jadonk/gsoc-application/pull/188>`_
* Submitted the project proposal using the reference template `Proposal <https://gsoc.beagleboard.io/proposals/template.html>`_

About 
=====

- **Forum:** :fab:`discourse` `u/vaishak_vidhyadharan (Vaishak Vidhyadharan) <https://forum.beagleboard.org/u/vaishak_vidhyadharan>`_
- **OpenBeagle:** :fab:`gitlab` `vvaishak (Vaishak Vidhyadharan) <https://openbeagle.org/vvaishak>`_
- **Github:** :fab:`github` `vaishakv (Vaishak Vidhyadharan) <https://github.com/vaishakv>`_
- **School:** :fas:`school` Open source beginner
- **Country:** :fas:`flag` Canada
- **Primary language:** :fas:`language` English
- **Typical work hours:** :fas:`clock` (Mon-Fri) 8.00PM-11.30PM, (Sat/Sun) 7.30AM-3.30PM Canada Eastern ST
- **Previous GSoC participation:** :fab:`google` N/A

Project
********

**Upstream Zephyr support for BBAI64 R5 Core:** The project focuses on adding upstream zephyr rtos support for ARM Cortex-R5 cores in J721E SoC.

Description
============

The BBAI-64 is a powerfull open source single board computer with massive computing power leveraging the TI TDA4VM SoC with 64bit ARM Cortex-A72 MPU subsystems, 
ARM Cortext-R5F MCU subsytems, DSP ,and GPU cores.

This proposal is to add upstream Zephyr RTOS support on ARM Cortex-R5F core in J721E SoC used in BBAI-64 single board computer.
During the GSoC development timeline, the goal is to add peripheral driver support for MailBox (inter processor communication), and
provide basic IPC examples to achieve communication between the Linux cores (ARM A72 Core) and the Zephyr running core (ARM R5F Core). 

The implementation should support the Linux running on A72 cores to interact with the remote ARM Cortex R5F cores using the remoteproc framework.
This provides the ability to power on, load firmware, power off to the R5F cores from Linux at runtime.

The previous contributor have added support for J721E board bringup and on chip peripheral drivers for Interrupt controller (VIM), UART, GPIO, TIMER. 
This support can be extended to MCSPI, as internal SPI is used as a main communication channel betwee the MAIN and MCU domain. Apart from this, the existing
peripherla drivers will be reviewed and any fixes identified will be contributed to the upstream repositories.  


Software
=========

* Zephyr RTOS
* C


Hardware
========

* BeagleBone AI64

Timeline
********

This section summaries the development time lines with 10 milestones, one for each week of development without 
an evaluation, and any pre-work. (A realistic, measurable timeline is critical to our selection process.)

.. note:: This timeline is based on the `official GSoC timeline <https://developers.google.com/open-source/gsoc/timeline>`_


Timeline summary
=================

.. table:: 

    +------------------------+----------------------------------------------------------------------------------------------------+
    | Date                   | Activity                                                                                           |                                  
    +========================+====================================================================================================+
    | March 16 - March 25    | Connect with possible mentors and request review on first draft                                    |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | March 26               | Complete prerequisites, verify value to community and request review on second draft               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | March 31               | Finalized timeline and request review on final draft                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | March 31 - April 2     | Submit application                                                                                 |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | May 1                  | Accepted GSoC contributor projects announced and start bonding.                                    |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | May 27                 | Coding officially begins. Start working on the introductory video.                                 |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 3                 | Release introductory video and complete milestone #1                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 10                | Complete milestone #2                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 17                | Complete milestone #3                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | June 24                | Complete milestone #4                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 1                 | Complete milestone #5                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 8                 | Submit midterm evaluations                                                                         |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 12                | Midterm evaluation deadline (standard coding period)                                               |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 15                | Complete milestone #6                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 22                | Complete milestone #7                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | July 29                | Complete milestone #8                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 5               | Complete milestone #9                                                                              |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 12              | Complete milestone #10                                                                             |
    +------------------------+----------------------------------------------------------------------------------------------------+
    | August 19 - August 26  | Submit final project video, submit final work to GSoC site and complete final mentor evaluation    |
    +------------------------+----------------------------------------------------------------------------------------------------+

Timeline detailed
=================

Community Bonding Period (May 1st - May 26th)
==============================================

- Start bonding with the mentors and community.
- Read documentations ( TRMs, Datasheets etc ) 
- Understand more on the requirements, receive and setup the BBAI-64 development board.
- Review previous contributor commits, understand the code changes.
- Fork the required repositories and consult with mentors on the review process for each milestone commits.

Coding begins (May 27th)
=========================

- Getting started with the BBAI-64 development board.
- Boot the custom built Linux on the A72 core.
- Understand more on the IPC mechanisms in J721E Soc from TRM, MCU domain and how it interconnects with the MAIN domain.
- Understand more on McSPI, and other peripheral subsystems in MCU domain.
- Understand more on RPMsg framework, remoteproc, virtio concepts.
- Understand on OpenAMP and its integration to Zephyr framework.
- Start working on the introductory video.

Milestone #1, Introductory YouTube video (June 3rd)
===================================================

- Publish the introductory video on YouTube
- Follow Zephyr getting started guide, and complete the following,
  #. Setup the Zephyr development environment on development Linux host machine.
  #. Install the Zephyr SDK and toolchains.
  #. Build the sample examples using west.
- Refer to the existing support for Beagle platforms in Zephyr and work on adding
  Kconfig, CMakefile, DT files to add build support to BBAI-64 R5F core platform in Zephyr.
- Understand more on the Zephyr subsystem and adding drivers to the existing Zephyr stack.

Milestone #2 (June 10th)
==========================

- Understand on the GPIO subsystem usage from the R5F core, configure a GPIO to output and 
  connect a LED.
- Build a sample blinky application in Zephyr and execute on BBAI-64 R5F core.
- Setup to debug the *blinky* application using gdb, add breakpoints, and get familiar with
  on debugging the application on R5F cores.
- Starting developing a sample OpenAMP IPC application for BBAI-64 R5F core by referring to
  other IPC sample source code available in Zephyr.

Milestone #3 (June 17th)
=========================

- Finalizae on the IPC sample application in Zephyr.
- Start developing a sample Linux remoteproc IPC application for A72 core.
- Understand on the mailbox driver support in Linux for BBAI-64

Milestone #4 (June 24th)
==========================

- Enable/Add the mailbox driver support in Linux for BBAI-64
- Understand mailbox driver support in Zephyr subsystem

Milestone #5 (July 1st)
========================

- Enable the mailbox driver in Zephyr for the BBAI-64 R5F core
- Finalize on the sample IPC application on Linux and Zephyr
- Start testing the IPC send and receive communication using the mailbox
- Finalize the changes, and add documnetation.

Submit midterm evaluations (July 8th)
=====================================

.. important:: 
    
    **July 12 - 18:00 UTC:** Midterm evaluation deadline (standard coding period) 

Milestone #6 (July 15th)
=========================

- Understand on TI SCI interface, and start working on adding support in Zephyr to use the TI-SCI interface.

Milestone #7 (July 22nd)
=========================

- Finalize on the TI-SCI support in Zephyr.
- Start working on device manager to send requests to R5F cores to control clocks, power management.
- Code clean up and update documentation

Milestone #8 (July 29th)
=========================

- Finalize the changes required to support device manager to communicate with R5F cores.
- Start working on understanding the McSPI peripheral subsystem

Milestone #9 (Aug 5th)
=======================

- Add support for McSPI peripheral driver on Zephyr
- Sample application code to demonstarte the SPI Rx/Tx


Milestone #10 (Aug 12th)
========================

- Finalize the changes for McSPI driver and sample code.
- Review the code changes, code cleanup and update documentation.
- Finalize the commits for the final PR and start working on final youtube video.

Final YouTube video (Aug 19th)
===============================

- Finalize and submit the final project video.
- Submit final work to GSoC site and complete final mentor evaluation.

Final Submission (Aug 24nd)
============================

.. important::

    **August 19 - 26 - 18:00 UTC:** Final week: GSoC contributors submit their final work 
    product and their final mentor evaluation (standard coding period)

    **August 26 - September 2 - 18:00 UTC:** Mentors submit final GSoC contributor 
    evaluations (standard coding period)

Initial results (September 3)
=============================

.. important:: 
    **September 3 - November 4:** GSoC contributors with extended timelines continue coding

    **November 4 - 18:00 UTC:** Final date for all GSoC contributors to submit their final work product and final evaluation

    **November 11 - 18:00 UTC:** Final date for mentors to submit evaluations for GSoC contributor projects with extended deadline

Experience and approch
***********************

- Handled various Linux device drivers in the past for networking, video subsystems with various SoC vendors.
- Experienced in bringing up the driver support for peripherals like SPI, I2C, Ethernet.
- Knowledge on Linux device drivers and kernel internals
- Knowledge on 32bit RISC based microcontrollers, TRMs, datasheets, schematics for various projects.

Contingency
===========

In the situation where the mentor is not available, I will be relying on the online resources, community support and my past development experience
to figure out a solution. 

Benefit
========

- The aim of this proposal is to add more peripheral driver support in Zephyr for BBAI-64 platform which helps
  the open source community to use the software freely to experiement/learn/build customized projects.

- For organizations which rely on open source hardware and software, can easily prototype a product using BBAI-64 and Zephyr,
  customize, enhance it as per the customer need with less cost.

Misc
====

